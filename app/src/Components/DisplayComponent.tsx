import { useState } from "react"
import './display.css';
import '../Fonts/Shockwave.ttf';
import { useEffect } from "react";
import { api, ShowViewModel } from "../Services/Api";
import { config } from "../Services/Config";

export const DisplayComponent = () => {

    const [scene, setScene] = useState<number>(1);

    const onStateChanged = (show: ShowViewModel) => {
        setScene(show.currentScene);
    }

    useEffect(() => {
        api.getShow(config.defaultShowName)
            .then(response => {
                setScene(response.show.currentScene);
            })
            .then(() => api.subscribeToStateChange(config.defaultShowName, onStateChanged));
    }, []);

    return <div className="scene-display-container scene-display text-center">
        <div className="scene-display-center">
            {scene}
        </div>
    </div>
}