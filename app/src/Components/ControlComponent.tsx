import { Button, Typography } from "@material-ui/core";
import { useEffect, useState } from "react";
import { api, ShowViewModel } from "../Services/Api";
import { config } from "../Services/Config";

import './control.css';

export const ControlComponent = () => {

    const [scene, setScene] = useState<number>(1);

    const onStateChanged = (show: ShowViewModel) => {
        setScene(show.currentScene);
    }

    useEffect(() => {
        api.getShow(config.defaultShowName)
            .then(response => {
                setScene(response.show.currentScene);
            })
            .then(() => api.subscribeToStateChange(config.defaultShowName, onStateChanged));
    }, []);

    const onIncreasedClicked = () => api.increaseCounter(config.defaultShowName, {});
    const onDecreasedClicked = () => api.decreaseCounter(config.defaultShowName, {});
    const onResetClicked = () =>  api.resetCounter(config.defaultShowName, {});

    return <div className="container">
        <div className="col mt-3 mb-2 text-center">
            <Button variant="contained" className="w-50" color="primary" onClick={onIncreasedClicked}>Incrémenter</Button>
        </div>
        <div className="col my-2 text-center">
            <Button variant="contained" className="w-50" color="primary" onClick={onDecreasedClicked}>Décrémenter</Button>
        </div>

        <div className="control-panel-btn-spacer"/>

        <div className="col my-2 text-center">
            <Button variant="contained" className="w-50" color="primary" onClick={onResetClicked}>Réinitialiser</Button>
        </div>

        <div className="control-panel-display-spacer"/>

        <div className="col text-center">
            <Typography variant="h3">Scène : {scene}</Typography>
        </div>
    </div>;
}