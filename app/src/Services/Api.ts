import { HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { config } from "./Config";

export class Api {
    
    private backendUrl: string;

    constructor() {
        this.backendUrl = config.backendUrl;
    }

    getShow(showId: string) : Promise<GetShowResponse> {
        const url: string = this.backendUrl + `Trente/${showId}`;

        const request: RequestInit = {
            method: 'GET'
        };

        return fetch(url, request)
            .then(response => response.json());
    }

    increaseCounter(showId: string, body: IncreaseCounterRequest) : Promise<IncreaseCounterResponse> {
        const url: string = this.backendUrl + `Trente/${showId}/increase-counter`;

        const request: RequestInit = {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {"Content-type": "application/json"}
        };

        return fetch(url, request)
            .then(response => response.json());
    }

    decreaseCounter(showId: string, body: DecreaseCounterRequest) : Promise<DecreaseCounterResponse> {
        const url: string = this.backendUrl + `Trente/${showId}/decrease-counter`;

        const request: RequestInit = {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {"Content-type": "application/json"}
        };

        return fetch(url, request)
            .then(response => response.json());
    }

    resetCounter(showId: string, body: ResetCounterRequest) : Promise<ResetCounterResponse> {
        const url: string = this.backendUrl + `Trente/${showId}/reset-counter`;

        const request: RequestInit = {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {"Content-type": "application/json"}
        };

        return fetch(url, request)
            .then(response => response.json());
    }

    subscribeToStateChange(showId: string, callback: (show: ShowViewModel) => void) : Promise<void>{
        const eventHub = new HubConnectionBuilder()
            .configureLogging(LogLevel.Information)
            .withUrl(config.backendUrl + "trente-hub/")
            .withAutomaticReconnect()
            .build();

        eventHub.on("stateChanged", result => callback(result));
        
        return eventHub.start()
            .then(() => eventHub!.invoke("subscribe", showId));
    }
}

export const api = new Api();

export interface ShowViewModel {
    readonly currentScene: number;
}

export interface GetShowResponse {
    readonly show: ShowViewModel;
}

export interface IncreaseCounterRequest {}
export interface IncreaseCounterResponse {
    readonly show: ShowViewModel;
}

export interface DecreaseCounterRequest {}
export interface DecreaseCounterResponse {
    readonly show: ShowViewModel;
}

export interface ResetCounterRequest {}
export interface ResetCounterResponse {
    readonly show: ShowViewModel;
}