import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { ControlComponent } from './Components/ControlComponent';
import { DisplayComponent } from './Components/DisplayComponent';

import './style.css';

const App = () => <div className="app p-1">
    <BrowserRouter>
      <Switch>
        <Route exact path={["/", "/display"]} component={DisplayComponent}/>
        <Route exact path={["/", "/control"]} component={ControlComponent}/>
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  </div>

export default App;